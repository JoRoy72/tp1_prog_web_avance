from flask import Flask
from flask import render_template
from flask import redirect
from flask import request
from flask import make_response
import datetime
from datetime import date
from datetime import timedelta
import sqlite3

app = Flask(__name__)

nom_utilisateur = ""

@app.route('/')
def home():
    return render_template('login.html')


@app.route('/', methods=['POST'])
def login():
    matricule = request.form['matricule']
    erreur = ""
    if len(matricule) != 6:
        erreur = "Matricule invalide! Format attendu: XXX-99"
    else:
        if not valider_matricule(matricule):
            erreur = "Matricule invalide! Format attendu: XXX-99"
    if erreur == "":
        date_du_jour = date.today().isoformat()
        global nom_utilisateur
        nom_utilisateur = matricule
        return redirect('/%s/%s' % (matricule, date_du_jour))
    else:
        return render_template('login.html', erreur=erreur)


@app.route('/<matricule>/<date_du_jour>')
def index(matricule, date_du_jour):
    reponse = ""
    date_du_jour = ajuster_date(date_du_jour)
    if valider_mat_date(matricule, date_du_jour):
        connexion = sqlite3.connect("db.db")
        curseur = connexion.cursor()
        curseur.execute("""select * from heures where date_publication = ?
            and matricule = ?""", (date_du_jour, matricule))
        listeHeures = []
        for ligne in curseur:
            listeHeures.append(ligne)
        reponse = make_response(render_template('index.html',
            date_menu=date.today().isoformat(),
            matricule=matricule,
            date_du_jour=date_du_jour,
            listeHeures=listeHeures))
    else:
        reponse = make_response(redirect('/'))
    return reponse


@app.route('/<matricule>/<date_du_jour>/modifier/<id>', methods=['POST', 'GET'])
def modifier(matricule, date_du_jour, id):
    reponse = ""
    date_du_jour = ajuster_date(date_du_jour)
    if request.method == "GET":
        if valider_mat_date(matricule, date_du_jour):
            connexion = sqlite3.connect("db.db")
            curseur = connexion.cursor()
            curseur.execute("""select * from heures where 
                date_publication = ?
                and matricule = ?
                and id = ?""", (date_du_jour, matricule, id))
            infos = []
            for ligne in curseur:
                infos.append(ligne)
            if len(infos) > 0:
                reponse = make_response(render_template('modifier.html',
                    date_menu=date.today().isoformat(),
                    infos=infos,
                    matricule=matricule,
                    date_du_jour=date_du_jour))
            else:
                reponse = make_response(redirect('/%s/%s' % (matricule, date_du_jour)))
        else:
            reponse = make_response(redirect('/'))
    else:
        projet = ''.join(request.form['projet'].split())
        erreur = valider_formulaire(request.form['matricule'],
            request.form['date'], projet, request.form['duree'])
        if len(erreur) > 0:
            infos = [[id, request.form['matricule'], request.form['projet'], request.form['date'], request.form['duree']]]
            reponse = render_template('modifier.html',
                erreur=erreur,
                date_menu=date.today().isoformat(),
                matricule=matricule,
                date_du_jour=date_du_jour,
                infos=infos)
        else:
            connexion = sqlite3.connect("db.db")
            curseur = connexion.cursor()
            curseur.execute("UPDATE heures SET matricule = ?, code_de_projet = ?, date_publication = ?, duree = ? WHERE id = ?",
                (request.form['matricule'], request.form['projet'], request.form['date'], request.form['duree'], id, ))
            connexion.commit()
            reponse = make_response(redirect('/%s/%s' % (matricule, request.form['date'])))
    return reponse


@app.route('/<matricule>/<date_du_jour>/ajout', methods=['GET', 'POST'])
def ajouter(matricule, date_du_jour):
    reponse = ""
    date_du_jour = ajuster_date(date_du_jour)
    if request.method == "GET":
        if valider_mat_date(matricule, date_du_jour):
            reponse = make_response(render_template('ajouter.html',
                matricule=matricule,
                date_menu=date.today().isoformat(),
                date_du_jour=date_du_jour,
                mat_form=matricule,
                date_form=date_du_jour))
        else:
            reponse = make_response(redirect('/%s/%s' % (matricule, date_du_jour)))
    else:
        projet = ''.join(request.form['projet'].split())
        erreur = valider_formulaire(request.form['matricule'],
            request.form['date'], projet, request.form['duree'])
        if len(erreur) > 0:
            reponse = render_template('ajouter.html',
                erreur=erreur,
                date_menu=date.today().isoformat(),
                matricule=matricule,
                date_du_jour=date_du_jour,
                mat_form=request.form['matricule'],
                date_form=request.form['date'],
                projet=projet,
                duree=request.form['duree'])
        else:
            id = generer_id()
            connexion = sqlite3.connect("db.db")
            curseur = connexion.cursor()
            curseur.execute("INSERT INTO heures values(?, ?, ?, ?, ?)",
                (id, request.form['matricule'], request.form['projet'], request.form['date'], request.form['duree'], ))
            connexion.commit()
            reponse = make_response(redirect('/%s/%s' % (matricule, request.form['date'])))
    return reponse


@app.route('/<matricule>/<date_du_jour>/supprimer/<id>', methods=['GET'])
def supprimer(matricule, date_du_jour, id):
    reponse = ""
    date_du_jour = ajuster_date(date_du_jour)
    if valider_mat_date(matricule, date_du_jour):
        connexion = sqlite3.connect("db.db")
        curseur = connexion.cursor()
        curseur.execute("DELETE FROM heures WHERE id = ? and matricule = ? and date_publication = ?",
            (id, matricule, date_du_jour))
        connexion.commit()
        reponse = make_response(redirect("/%s/%s" % (matricule, date_du_jour)))
    else:
        reponse = make_response(redirect('/'))
    return reponse


@app.route('/<matricule>/<date_du_jour>/precedent', methods=['GET'])
def jour_precedent(matricule, date_du_jour):
    reponse = ""
    date_du_jour = ajuster_date(date_du_jour)
    if valider_mat_date(matricule, date_du_jour):
        jour_present = datetime.datetime.strptime(date_du_jour, "%Y-%m-%d")
        jour_prec = jour_present - datetime.timedelta(days=1)
        reponse = make_response(redirect('/%s/%s' % (matricule, jour_prec.date())))
    else:
        reponse = make_response(redirect('/'))
    return reponse


@app.route('/<matricule>/<date_du_jour>/suivant', methods=['GET'])
def jour_suivant(matricule, date_du_jour):
    reponse = ""
    date_du_jour = ajuster_date(date_du_jour)
    if valider_mat_date(matricule, date_du_jour):
        jour_present = datetime.datetime.strptime(date_du_jour, "%Y-%m-%d")
        jour_suiv = jour_present + datetime.timedelta(days=1)
        reponse = make_response(redirect('/%s/%s' % (matricule, jour_suiv.date())))
    else:
        reponse = make_response(redirect('/'))
    return reponse


@app.route('/<matricule>')
def sommaire(matricule):
    reponse = ""
    if valider_matricule(matricule) and matricule == nom_utilisateur:
        connexion = sqlite3.connect("db.db")
        curseur = connexion.cursor()
        curseur.execute("""SELECT date_publication FROM heures
            WHERE matricule = ? ORDER BY date_publication DESC""", (matricule, ))
        les_mois = []
        for ligne in curseur:
            date_complete = ligne[0]
            annee, mois, _ = date_complete.split('-')
            nom = mois_francais(int(mois))
            if not [annee, mois, nom] in les_mois:
                les_mois.append([annee, mois, nom])
        reponse = make_response(render_template("sommaire.html",
            date_menu=date.today().isoformat(), matricule=matricule, mois=les_mois))
    else:
        reponse = make_response(redirect('/'))
    return reponse


@app.route('/<matricule>/overview/<mois>')
def overview(matricule, mois):
    reponse = ""
    mois_complet = mois + "-01"
    mois_complet = ajuster_date(mois_complet)
    if valider_mat_date(matricule, mois_complet):
        a, m, j = mois_complet.split('-')
        mois_courant = date(int(a), int(m), int(j))
        nom_mois = mois_francais(int(m))
        un_jour = datetime.timedelta(days=1)
        jour_suivant = mois_courant
        calendrier = []
        jour_de_semaine = mois_courant.weekday()
        for _ in range(0, jour_de_semaine):
            calendrier.append("")
        while jour_suivant.month == mois_courant.month:
            connexion = sqlite3.connect("db.db")
            curseur = connexion.cursor()
            curseur.execute("""SELECT duree FROM heures
                WHERE matricule = ? and date_publication = ?""", (matricule, jour_suivant.isoformat(), ))
            nb_minutes = 0
            for ligne in curseur:
                nb_minutes += int(ligne[0])
            str_jour = str(jour_suivant.day)
            if jour_suivant.day < 10:
                str_jour = "0" + str_jour
            calendrier.append([nb_minutes, str_jour])
            jour_suivant += un_jour
        jour_de_semaine = jour_suivant.weekday()
        if jour_de_semaine != 0:
            for _ in range(jour_de_semaine, 7):
                calendrier.append("")
        reponse = render_template('calendrier.html', 
            date_menu=date.today().isoformat(), matricule=matricule, mois=mois, annee=a, nom_mois=nom_mois, calendrier=calendrier)
    else:
        reponse = redirect('/%s' % matricule)
    return reponse


@app.route('/<matricule>/overview/<mois>/suivant', methods=['GET'])
def prochain_mois(matricule, mois):
    reponse = ""
    date_du_jour = mois + "-28"
    date_du_jour = ajuster_date(date_du_jour)
    if valider_mat_date(matricule, date_du_jour):
        a, m, j = date_du_jour.split('-')
        mois_courant = date(int(a), int(m), int(j))
        un_jour = datetime.timedelta(days=1)
        jour_suivant = mois_courant
        while jour_suivant.month == mois_courant.month:
            jour_suivant += un_jour
        a = jour_suivant.year
        m = jour_suivant.month
        nouveau_mois = ""
        if m < 10:
            nouveau_mois = str(a) + "-0" + str(m)
        else:
            nouveau_mois = str(a) + "-" + str(m)
        reponse = make_response(redirect('/%s/overview/%s' % (matricule, nouveau_mois)))
    else:
        reponse = make_response(redirect('/'))
    return reponse


@app.route('/<matricule>/overview/<mois>/precedent', methods=['GET'])
def mois_precedent(matricule, mois):
    reponse = ""
    date_du_jour = mois + "-01"
    date_du_jour = ajuster_date(date_du_jour)
    if valider_mat_date(matricule, date_du_jour):
        a, m, j = date_du_jour.split('-')
        mois_courant = date(int(a), int(m), int(j))
        un_jour = datetime.timedelta(days=1)
        jour_precedent = mois_courant
        jour_precedent -= un_jour
        a = jour_precedent.year
        m = jour_precedent.month
        nouveau_mois = ""
        if m < 10:
            nouveau_mois = str(a) + "-0" + str(m)
        else:
            nouveau_mois = str(a) + "-" + str(m)
        reponse = make_response(redirect('/%s/overview/%s' % (matricule, nouveau_mois)))
    else:
        reponse = make_response(redirect('/'))
    return reponse


def valider_matricule(champ):
    resultat = True
    matr = champ.split('-')
    if len(matr) == 2:
        if matr[0].isalpha() and len(matr[0]) == 3:
            if not matr[1].isdigit() or len(matr[1]) != 2:
                resultat = False
        else:
            resultat = False
    else:
        resultat = False
    return resultat


def valider_mat_date(mat, la_date):
    reponse = True
    print("Nom d'utilisateur: %s" % nom_utilisateur)
    if valider_matricule(mat) and mat == nom_utilisateur:
        jour = la_date.split('-')
        if len(jour) == 3:
            try:
                date(int(jour[0]), int(jour[1]), int(jour[2]))
            except ValueError:
                reponse = False
        else:
            reponse = False
    else:
        reponse = False
    return reponse


def valider_formulaire(matricule, date_du_jour, projet, duree):
    erreur = []
    if not valider_matricule(matricule) or matricule == "":
        erreur.append("Matricule invalide! Format attendu: XXX-99")
    if projet == "":
        erreur.append("Le code du projet est manquant!")
    if date_du_jour == "":
        erreur.append("Date manquante!")
    else:
        jour = date_du_jour.split('-')
        if len(jour) == 3:
            try:
                date(int(jour[0]), int(jour[1]), int(jour[2]))
            except ValueError:
                erreur.append("Date invalide! Format attendu: YYYY-MM-JJ")
        else:
            erreur.append("Date invalide! Format attendu: YYYY-MM-JJ")
    if not duree.isdigit() or duree == "":
        erreur.append("Le temps travaillé doit être un nombre!")
    return erreur


def generer_id():
    connexion = sqlite3.connect("db.db")
    curseur = connexion.cursor()
    curseur.execute("SELECT id FROM heures")
    i = 1
    deja_trouve = False
    for ligne in curseur:
        if not deja_trouve:
            if ligne[0] != i:
                deja_trouve = True
            else:
                i += 1
    return i


def ajuster_date(date):
    elem_date = date.split("-")
    for i in range(0, len(elem_date)):
        if len(elem_date[i]) == 1:
            elem_date[i] = "0" + elem_date[i]
    return '-'.join(elem_date)


def mois_francais(mois):
    reponse = mois
    if mois == 1:
        reponse = "Janvier"
    elif mois == 2:
        reponse = "Février"
    elif mois == 3:
        reponse = "Mars"
    elif mois == 4:
        reponse = "Avril"
    elif mois == 5:
        reponse = "Mai"
    elif mois == 6:
        reponse = "Juin"
    elif mois == 7:
        reponse = "Juillet"
    elif mois == 8:
        reponse = "Août"
    elif mois == 9:
        reponse = "Septembre"
    elif mois == 10:
        reponse = "Octobre"
    elif mois == 11:
        reponse = "Novembre"
    elif mois == 12:
        reponse = "Décembre"
    return reponse